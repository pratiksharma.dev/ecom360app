<img alt='log' src="client/public/icons/android-chrome-192x192.png" style='height:48px' />

It is an MERN ( mongodb, express, react, node) stack e-commerce app. This app includes lots of functionality like user dashboard, admin dashboard, create-product, create-category, manage order, buy books, profile management, cart. 

#### `Tech Stack:`

[![Generic badge](https://img.shields.io/badge/Node.js->=10-red.svg)](https://shields.io/)  [![Generic badge](https://img.shields.io/badge/React.js->=16.8-blue.svg)](https://shields.io/)  [![Generic badge](https://img.shields.io/badge/MongoDB->=4-teal.svg)](https://shields.io/)  [![Generic badge](https://img.shields.io/badge/Express.js->=4-<COLOR>.svg)](https://shields.io/) [![Generic badge](https://img.shields.io/badge/Braintree.js->=2-yellow.svg)](https://shields.io/)



#### `How to run locally?`

```bash

```
# 'External script to add favorite for a user'
<!-- var url_string = window.location.href
var url = new URL(url_string);
var userId = url.searchParams.get("userId");
var xhr = new XMLHttpRequest();
xhr.open("POST", (`http://localhost:8000/api/favorite/createOrUpdate/`+userId), true);
xhr.setRequestHeader('Content-Type', 'application/json');
xhr.send(JSON.stringify({"user":userId,
"products":{"productId":"5fcce061b60d884ec4f4fa4b"}}
)); -->



#### `.env structure`

```bash
MONGO_URI=<MONGO_DB_SERVER>
NODE_ENV=production  # `production` or `dev`
CLIENT_URL=<REACT_APP_SERVER>
PORT=8000
BRAINTREE_ID=<BRAINTREE_SANDBOX_ID>
BRAINTREE_PRIVATE=<BRAINTREE_SANDBOX_PRIVATE_ID>
BRAINTREE_PUBLIC=<BRAINTREE_SANDBOX_PUBLIC_ID>
JWT=<JWT_SECRET>
```



 [![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)  [![forthebadge](https://forthebadge.com/images/badges/made-with-javascript.svg)](https://forthebadge.com)