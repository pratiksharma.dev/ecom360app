import React, { useState, useEffect } from "react";
import Layout from "../core/Layout";
import { isAuthenticated } from "../auth";
import { Link } from "react-router-dom";
import { createCompany } from "./apiAdmin"

const AddCompany = () => {
  const [values, setValues] = useState({
    name: "",
    description: "",
    companyEmail: "",
    photo: "",
    virtualTourUrl:"",
    loading: false,
    error: "",
    createdCompany: "",
    redirectToProfile: false,
    formData: "",
  });

  const { user, token } = isAuthenticated();
  // eslint-disable-next-line
  const {
    name,
    description,
    companyEmail,
    virtualTourUrl,
    loading,
    error,
    createdCompany, // eslint-disable-next-line
    redirectToProfile,
    formData,
  } = values;

  // set form data
  const init = () => {
    
    setValues({
      ...values,
      formData : new FormData()
    });
  };

  useEffect(() => {
    init(); // eslint-disable-next-line
  }, []);

  const handleChange = (name) => (event) => {
    const value = name === "photo" ? event.target.files[0] : event.target.value;
    formData.append(name, value);
    setValues({ ...values, [name]: value });
  };

  const clickSubmit = (event) => {
    event.preventDefault();
    setValues({ ...values, error: "", loading: true });
    createCompany(user._id, token, formData).then((data) => {
      if (data && data.error) {
        setValues({ ...values, error: data.error });
      } else {
        setValues({
          ...values,
          name: "",
          description: "",
          companyEmail: "",
          virtualTourUrl: "",
          photo: "",
          loading: false,
          createdCompany: data.name,
        });
      }
    });
  };
  const goBack = () => (
    <div className="m-2">
      <Link to="/admin/dashboard" className="text-warning">
        Back to Dashboard
      </Link>
    </div>
  );
  const newPostForm = () => (
    <form align="left" className="card w-75 w-sm-100" onSubmit={clickSubmit}>
      <div className="card-body">
        <h4>Company Store Photo</h4>
        <div className="form-group">
          <div className="custom-file">
            <input
              onChange={handleChange("photo")}
              name="photo"
              accept="image/*"
              type="file"
              
              className="custom-file-input"
              id="inputGroupFile01"
              aria-describedby="inputGroupFileAddon01"
            />
            <label className="custom-file-label" htmlFor="inputGroupFile01">
              Choose file
            </label>
          </div>
        </div>

        <div className="form-group">
          <label className="text-muted">Company Name</label>
          <input
            onChange={handleChange("name")}
            type="text"
            required
            className="form-control"
            value={name}
          />
        </div>

        <div className="form-group">
          <label className="text-muted">Description</label>
          <textarea
            onChange={handleChange("description")}
            rows="2"
            className="form-control"
            value={description}
          />
        </div>

        <div className="form-group">
          <label className="text-muted">Company Email</label>
          <textarea
            onChange={handleChange("companyEmail")}
            required
            rows="1"
            className="form-control"
            value={companyEmail}
          />
        </div>
        <div className="form-group">
          <label className="text-muted">Virtual Tour Address</label>
          <textarea
            onChange={handleChange("virtualTourUrl")}
            required
            rows="3"
            className="form-control"
            value={virtualTourUrl}
          />
        </div>

        <button className="btn btn-outline-primary">Create Company</button>
      </div>
    </form>
  );

  const showError = () => (
    <div
      className="alert alert-danger"
      style={{ display: error ? "" : "none" }}
    >
      {error}
    </div>
  );

  const showSuccess = () => (
    <div
      className="alert alert-info"
      style={{ display: createdCompany ? "" : "none" }}
    >
      <h2>{`${createdCompany}`} is created!</h2>
    </div>
  );

  const showLoading = () =>
    loading && (
      <div className="alert alert-success">
        <h2>Loading...</h2>
      </div>
    );

  return (
    <Layout
      title="Add a new company"
      className="container"
      description={`G'day ${user.name}, ready to add a new compnany?`}
    >
      <div className="my-5">
        <div align="center" className="container">
          {goBack()}
          {showLoading()}
          {showSuccess()}
          {showError()}
          {newPostForm()}
        </div>
      </div>
    </Layout>
  );
};

export default AddCompany;
