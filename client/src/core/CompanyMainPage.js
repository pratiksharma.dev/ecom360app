import React, { useState, useEffect } from "react";
//import { NavLink } from "react-router-dom";
import { isAuthenticated } from "../auth";
import {getCompanyByEmail, getCompany} from "./apiCore"
import CompanyPageLayout from "./CompanyPageLayout";
import LandingPageBackground from "../assets/LandingPageBackground.png";

const CompanyMainPage = (props) => {
  const [companyImage, setCompanyImage] = useState();
  const [companyUrl, setCompanyUrl] = useState(); // eslint-disable-next-line
  const [companyId, setCompanyId] = useState();
  const [error, setError] = useState(false);
  const [load, setLoad] = useState(false);

  const { user } = isAuthenticated();

  const loadCompanyDetails = () => {
    
    if(user){
      setLoad(true);
      getCompanyByEmail(user.email).then((data) => {
        if (data && data.error) {
          setError(data.error);
        } else {
          setCompanyUrl(data.virtualTourUrl);
          setLoad(false);
        }
      });
    } else if(props.match.params.companyId) {
      getCompany(props.match.params.companyId).then((data) => {
        if (data && data.error) {
          setError(data.error);
        } else {
          setCompanyUrl(data.virtualTourUrl);
          setLoad(false);
        }
      });
    }
  };
  useEffect(() => {
    setCompanyId(props.match.params.companyId);
    loadCompanyDetails();
  }, []);
  const show = () => (
    <div>
      {companyUrl &&
      <div className="iframe-container">
        <iframe src={companyUrl}/>
      </div>}
      {!companyUrl && <div className="container-fluid">
        <div className="row">
          <div className="col-4">
            <img
              src={
                companyImage != undefined && companyImage != null
                  ? companyImage
                  : LandingPageBackground
              }
              alt="book"
              style={{ width: "100%" }}
            />
          </div>
          <div className="col-8">
            <h2 className="mb-4" style={{ fontSize: "3em" }}>
              Welcome to Virtual Shopping Experience
            </h2>

            {/* <div className="ui buttons">
              <NavLink
                style={{ color: "white" }}
                to={"/customerSignin/" + companyUrl}
              >
                <button className="landing-page-btn">Log In</button>
              </NavLink>
              <div className="or" style={{ height: "4em" }}></div>
              <NavLink style={{ color: "white" }} to="/signup">
                <button className="landing-page-btn sign-up">Sign Up</button>
              </NavLink>
              <div className="or" style={{ height: "4em" }}></div>
              <a style={{ color: "white" }} target="none" href={companyUrl}>
                <button className="landing-page-btn skip-btn">
                  Enter without Login
                </button>
              </a>
            </div> */}
          </div>
        </div>
      </div>}
    </div>
  );
  const loading = () => (
    <div className="ui segment">
      <div className="ui active inverted dimmer">
        <div className="ui large text loader">Loading</div>
      </div>
    </div>
  );
  return (
    <CompanyPageLayout
      title="View360"
      logo="true"
      className="container"
      description="Virtual Tour E-Commerce Dashboard"
    >
      <div className="my-4">{load === true ? loading() : show()}</div>
    </CompanyPageLayout>
  );
};

export default CompanyMainPage;
