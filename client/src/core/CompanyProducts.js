import React, { useState, useEffect } from "react";
import Layout from "./Layout";
import Card from "./Card";
import { getCategories, getFilteredProducts, getCompanyByEmail } from "./apiCore";
import Checkbox from "./Checkbox";
import RadioBox from "./RadioBox";
import {isAuthenticated} from "../auth"
import { prices } from "./fixedPrices";

const CompanyProducts = () => {
  const {user} = isAuthenticated();
  const [myFilters, setMyFilters] = useState({
    filters: { category: [], price: []},
  });
  const [company, setCompany] = useState({
    companyDetails: { id: ""},
  });
  const [categories, setCategories] = useState([]); // eslint-disable-next-line
  const [error, setError] = useState(false); // eslint-disable-next-line
  const [limit, setLimit] = useState(6);
  const [skip, setSkip] = useState(0);
  //const [company, setCompany] = useState("");
  const [filteredResults, setFilteredResults] = useState([]);
  const [size, setSize] = useState(0);
  
  const configureCompany = () => {
    getCompanyByEmail(user.email).then((data)=>{
      if (!data){
        setError("No company found for this account");
      }
      else if(data && data.error) {
        setError(data.error);
      } else {
        const cmp = { ...company };
        cmp.companyDetails["id"] = data._id;
        setCompany(cmp);
      }
      
    }, err => {
      setError("Error : ", err);
      console.log(err);
    }).then(()=>{
      configureCategories();
    });
  }
  const configureCategories = () => {
    getCategories().then((data) => {
      if (data && data.error) {
        setError(data.error);
      } else {
        setCategories(data);
      }
    }).then(()=>{
      loadFilteredResults(skip, limit, company.companyDetails.id, myFilters.filters); // eslint-disable-next-line
    });

    
  }

  const loadFilteredResults = (newFilters) => {
    getFilteredProducts(skip, limit, company.companyDetails.id, newFilters).then((data) => {
      if (data && data.error) {
        setError(data.error);
      } else {
        setFilteredResults(data.data);
        setSize(data.size);
        setSkip(0);
      }
    });
  };
  const loadMore = () => {
    let toSkip = skip + limit;

    getFilteredProducts(toSkip, limit, company.companyDetails.id, myFilters.filters).then((data) => {
      if (data && data.error) {
        setError(data.error);
      } else {
        if (data) {
          setFilteredResults([...filteredResults, ...data.data]);
          setSize(data.size);
          setSkip(0);
        }
      }
    });
  };
  const loadMoreButton = () => {
    return (
      size > 0 &&
      size >= limit && (
        <button onClick={loadMore} className="btn btn-warning mb-5">
          Load More
        </button>
      )
    );
  };
  useEffect(() => {
    configureCompany(); // eslint-disable-next-line
  }, []);

  const handleFilters = (filters, filterBy) => {
    const newFilters = { ...myFilters };
    newFilters.filters[filterBy] = filters;

    if (filterBy === "price") {
      let priceValues = handlePrice(filters);
      newFilters.filters[filterBy] = priceValues;
    }
    loadFilteredResults(myFilters.filters);
    setMyFilters(newFilters);
  };

  const handlePrice = (value) => {
    const data = prices;
    let array = [];

    for (let key in data) {
      if (data[key]._id === parseInt(value)) {
        array = data[key].array;
      }
    }
    return array;
  };

  return (
    <Layout
      title="Your Company's Products"
      description="Find all product of your company here"
      className="container"
    >
      <div className="my-4">
        <div className="row">
          <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12 mb-2">
            <div className="ui vertical menu">
              <div className="item">
                <div className="header">Filter by categories</div>
                <div className="menu">
                  <Checkbox
                    categories={categories}
                    handleFilters={(filters) =>
                      handleFilters(filters, "category")
                    }
                  />
                </div>
              </div>
              <div className="item">
                <div className="header">Filter by price range</div>
                <div className="menu">
                  <RadioBox
                    prices={prices}
                    handleFilters={(filters) => handleFilters(filters, "price")}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <h2 className="mb-2">Products</h2>
            <div className="row">
              {filteredResults.map((product, i) => (
                <div
                  key={i}
                  className="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-3"
                >
                  <Card 
                    removeButton={false}
                    cartButton={false}
                    cartUpdate={false}
                    product={product} 
                  />
                </div>
              ))}
            </div>
            <hr />
            {loadMoreButton()}
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default CompanyProducts;
