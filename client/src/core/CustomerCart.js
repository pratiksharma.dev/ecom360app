import React, { useState, useEffect } from "react";
import Layout from "./Layout";
import { getFavorite} from "./apiCore"
import Card from "./Card";

const CustomerCart = (props) => {
  const [items, setItems] = useState([]);
  const [error, setError] = useState({});

  useEffect(() => {
    const userId = props.match.params.userId;
    loadFavoriteProducts(userId);
  }, [props]);
    

  const loadFavoriteProducts = (userId) => {
    
    getFavorite(userId).then((data) => {
      if (data && data.error) {
        setError(data.error);
      } else {
        setItems(data.products);
      }
    });
  }

  // const configureProducts = (productList) => {
  

  const showItems = (items) => {
    return (
      <div>
        <h2>Customer's cart has {`${items.length}`} items</h2>
        <hr />
        <div className="row no-guttes">
          {items.map((product, i) => (
            <div key={i} className="col-6 mb-2">
              <Card
                removeButton={false}
                cartButton={false}
                cartUpdate={false}
                product={product.productId}
              />
            </div>
          ))}
        </div>
      </div>
    );
  };

  const noItemsMessage = () => (
    <h2>
      Customer's cart is empty. <br /> 
    </h2>
  );

  return (
    <Layout
      title="Shopping Cart"
      description="Manage customer cart items. Add or remove products."
      className="container"
    >
      <div className="my-5">
        <div className="row">
          <div className="col-lg-8 col-md-8 col-sm-8 col-xs-12 mb-2">
            {items.length > 0 ? showItems(items) : noItemsMessage()}
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default CustomerCart;
