import React, { useState, useEffect } from "react";
import {isAuthenticated} from "../auth"
import Layout from "./Layout";
import { Link } from "react-router-dom";
import { getCustomerDetails } from "./apiCore";


const CustomerDetails = () => {
  const [customerDetails, setCustomerDetails] = useState([]); //eslint-disable-next-line
  const [error, setError] = useState(false); // eslint-disable-next-line
  const {user} = isAuthenticated();
  
  const init = () => {
    getCustomerDetails(user._id).then((data) => {
      if (data && data.error) {
        setError(data.error);
      } else {
        setCustomerDetails(data);
      }
    });
  };

  useEffect(() => {
    init();
  }, []);

  return (
    <Layout
      title="Customer Details"
      description="Here is the list of all your customers"
      className="container"
    >
      <div className="my-4">
        <div className="row">
          <div className="col-sm-2">Serial Number</div>
          <div className="col-sm-6">Customer Name</div>
          <div className="col-sm-4">Action</div>
        </div>
        {customerDetails && customerDetails.map((customer, i) => (
          <div key={i} className="row">
            <br/>
            <div className="col-sm-2">{i + 1}</div>
            <div className="col-sm-6">{customer.member.name}</div>
            <div className="col-sm-4">
              <Link
                className="ui basic green button"
                to={`/favorite/${customer.member._id}`}
              >
                Check Details
              </Link>
            </div>
            <br/>
          </div>
        ))}

        {/* <div className="row">
          <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12 mb-2">
            <div className="ui vertical menu">
              <div className="item">
                <div className="header">Filter by categories</div>
                <div className="menu">
                  <Checkbox
                    categories={categories}
                    handleFilters={(filters) =>
                      handleFilters(filters, "category")
                    }
                  />
                </div>
              </div>
              <div className="item">
                <div className="header">Filter by price range</div>
                <div className="menu">
                  <RadioBox
                    prices={prices}
                    handleFilters={(filters) => handleFilters(filters, "price")}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <h2 className="mb-2">Customers</h2>
            <div className="row">
              {customerDetails.map((customer, i) => (
                <div
                  key={i}
                  className="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-3"
                >
                  <Card product={product} />
                </div>
              ))}
            </div>
          </div>
        </div> */}
      </div>
    </Layout>
  );
};

export default CustomerDetails;
