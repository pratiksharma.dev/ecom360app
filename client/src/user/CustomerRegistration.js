import React, { useState } from "react";
import Layout from "../core/Layout";
import { signup, isAuthenticated } from "../auth";
import { createCustomer } from "../admin/apiAdmin" 

const CustomerRegistration = () => {
  const [values, setValues] = useState({
    name: "",
    email: "",
    password: "",
    error: "",
    success: false,
    customerSuccess: false,
  });

  const { name, email, password, success, customerSuccess , error } = values;
  
  
  // destructure user and token from localstorage, ol
  const { user, token } = isAuthenticated();

  const handleChange = (name) => (event) => {
    setValues({ ...values, error: false, [name]: event.target.value });
  };

  const clickSubmit = (event) => {
    event.preventDefault();
    setValues({ ...values, error: false });
    var member;
    signup({ name, email, password }).then((data) => {
      if (data && data.error) {
        setValues({ ...values, error: data.error, success: false });
      } else {
        member = data.user._id;
        setValues({
          ...values,
          name: "",
          email: "",
          password: "",
          error: "",
          success: true,
        });
      }
    }).then(() => {
      console.log(member, user._id, values);
        var owner = user._id;
        createCustomer(user._id, token, { member, owner }).then((data) => {
          if (data && data.error) {
            setValues({ ...values, error: data.error, customerSuccess: false });
          } else {
            setValues({
              ...values,
              name: "",
              email: "",
              password: "",
              error: "",
              customerSuccess: true,
            });
          }
        });
    });

    //Then create custo
    // if (true) {
    //     console.log(member, user._id, values);
    //     var owner = user._id;
    //     createCustomer(user._id, token, { member, owner }).then((data) => {
    //       if (data && data.error) {
    //         setValues({ ...values, error: data.error, customerSuccess: false });
    //       } else {
    //         setValues({
    //           ...values,
    //           name: "",
    //           email: "",
    //           password: "",
    //           error: "",
    //           customerSuccess: true,
    //         });
    //       }
    //     });
    //   }
  };

  const signUpForm = () => (
    <div className="container w-50 w-sm-100">
      <form className="card" onSubmit={clickSubmit}>
        <div className="card-body">
          <p className="h4 mb-4">Add New Customer</p>
          <input
            onChange={handleChange("name")}
            value={name}
            type="text"
            required
            id="defaultLoginFormEmail"
            className="form-control mb-4"
            placeholder="Name"
          />
          <input
            onChange={handleChange("email")}
            value={email}
            type="email"
            required
            id="defaultLoginFormEmail"
            className="form-control mb-4"
            placeholder="E-mail"
          />
          <input
            onChange={handleChange("password")}
            value={password}
            type="password"
            required
            id="defaultLoginFormPassword"
            className="form-control mb-4"
            placeholder="Password"
          />
          <div className="d-flex justify-content-around">
            <div></div>
          </div>
          <button
            onClick={clickSubmit}
            className="btn btn-outline-info btn-block my-4"
            type="submit"
          >
            Register Customer
          </button>
          
        </div>
      </form>
    </div>
  );

  const showError = () => (
    <div
      className="alert alert-danger"
      style={{ display: error ? "" : "none" }}
    >
      {error}
    </div>
  );

  const showSuccess = () => (
    <div
      className="alert alert-info"
      style={{ display: success && customerSuccess ? "" : "none" }}
    >
      New customer is added. 
    </div>
  );

  return (
    <Layout
      title="Regsiter Customer"
      description="Add your customer here"
      className="container"
    >
      <div className="my-4">
        {showSuccess()}
        {showError()}
        {signUpForm()}
      </div>
    </Layout>
  );
};

export default CustomerRegistration;
