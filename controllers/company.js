const Company = require("../model/Company");
const { errorHandler } = require("../helpers/dbErrorHandler");
const formidable = require("formidable");
const _ = require("lodash");
const fs = require("fs");

exports.companyById = (req, res, next, id) => {
  Company.findById(id)
    .exec((err, company) => {
      if (err || !company) {
        return res.status(400).json({
          error: "company Not found",
        });
      }
      req.company = company;
      next();
    });
};
exports.read = (req, res) => {
  req.company.photo = undefined;
  return res.json(req.company);
};
exports.create = (req, res) => {
  let form = new formidable.IncomingForm();
  form.keepExtensions = true;
  form.parse(req, (err, fields, files) => {
    if (err) {
      return res.status(400).json({
        error: "Image Could not be uploaded",
      });
    }
    // check for fileds
    const { name, description, companyEmail } = fields;
    if (!name || !description || !companyEmail) {
      return res.status(400).json({
        error: "All fileds are required",
      });
    }
    let company = new Company(fields);
    if (files.photo) {
      //console.log('FILES PHOTO',files.photo) 1kb=100 1mb=100000
      if (files.photo.size > 300000) {
        return res.status(400).json({
          error: "Image Size should less then 2mb",
        });
      }

      company.photo.data = fs.readFileSync(files.photo.path);
      company.photo.contentType = files.photo.type;
    }
    company.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler(err),
        });
      }
      res.json(result);
    });
  });
};
exports.remove = (req, res) => {
  let company = req.company;
  company.remove((err, deletedCompany) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler(err),
      });
    }
    res.json({ message: "Company deleted" });
  });
};
exports.update = (req, res) => {
  let form = new formidable.IncomingForm();
  form.keepExtensions = true;
  form.parse(req, (err, fields, files) => {
    if (err) {
      return res.status(400).json({
        error: "Image Could not be uploaded",
      });
    }
    // check for fileds
    // const { name, description, price, category, quantity, shipping } = fields;
    // if (!name || !description || !price || !category || !shipping) {
    //   return res.status(400).json({
    //     error: `All fileds are required ${name},${description},${price},${category},${quantity},${shipping}`
    //   });
    // }
    let company = req.company;
    company = _.extend(company, fields);
    if (files.photo) {
      //console.log('FILES PHOTO',files.photo) 1kb=100 1mb=100000
      if (files.photo.size > 300000) {
        return res.status(400).json({
          error: "Image Size should less then 2mb",
        });
      }

      company.photo.data = fs.readFileSync(files.photo.path);
      company.photo.contentType = files.photo.type;
    }
    company.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler(err),
        });
      }
      res.json(result);
    });
  });
};

// sell/arrival
// sell= /products?sortBy=sold&order=desc&limit=4
// arrival= /products?sortBy=createdAt&order=desc&limit=4
// if no params are sent return all products

exports.list = (req, res) => {
  let order = req.query.order ? req.query.order : "asc";
  let sortBy = req.query.sortBy ? req.query.sortBy : "_id";

  Company.find()
    .sort([[sortBy, order]])
    .exec((err, company) => {
      if (err) {
        return res.status(400).json({
          error: "Company not found",
        });
      }
      res.json(company);
    });
};

exports.companyByEmail = (req,res) => {
  Company.findOne({companyEmail:req.params.email})
  .exec((err, company)=>{
    if(err) {
      return res.status(400).json({
        error: "Company not found",
      });
    }
    res.json(company);
  });
};


exports.photo = (req, res, next) => {
  if (req.company.photo.data) {
    res.set("Content-Type", req.company.photo.contentType);
    return res.send(req.company.photo.data);
  }
  next();
};


