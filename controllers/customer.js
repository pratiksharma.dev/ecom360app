const Customer = require("../model/Customer");
const { errorHandler } = require("../helpers/dbErrorHandler");

exports.create = (req, res) => {
  const customer = new Customer(req.body);
  customer.save((err, data) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler(err)
      });
    }
    res.json({ data });
  });
};

exports.customerById=(req,res,next,id)=>{
  Customer.findById(id).exec((err,customer)=>{
      if(err || !customer){
          return res.status(400).json({
              error:"Customer Not found"
          });
      }
      req.customer=customer;
      next();
  })
}

exports.read = (req, res) => {
  return res.json(req.customer);
};

exports.update = (req, res) => {
  const customer = req.customer;
  customer.name = req.body.name;
  customer.save((err, data) => {
      if (err) {
          return res.status(400).json({
              error: errorHandler(err)
          });
      }
      res.json(data);
  });
};

exports.remove = (req, res) => {
  const customer = req.customer;
  customer.remove((err, data) => {
      if (err) {
          return res.status(400).json({
              error: errorHandler(err)
          });
      }
      res.json({
          message: "Customer deleted"
      });
  });
};

exports.list = (req, res) => {
  console.log("Owner", req.params.ownerId)
  Customer.find({owner:req.params.ownerId})
  .populate("member")
  .exec((err, data) => {
      if (err) {
          return res.status(400).json({
              error: errorHandler(err)
          });
      }
      res.json(data);
  });
};