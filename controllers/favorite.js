const { Favorite } = require("../model/Favorite");
const { errorHandler } = require("../helpers/dbErrorHandler");

exports.favoriteById = (req, res, next, id) => {
  Favorite.findById(id)
    .populate("products.product", "name price")
    .exec((err, favorite) => {
      if (err || !favorite) {
        return res.status(400).json({
          error: errorHandler(err),
        });
      }
      req.favorite = favorite;
      next();
    });
};
exports.create = (req, res) => {
  const favorite = new Favorite(req.body);
  favorite.save((error, data) => {
    if (error) {
      return res.status(400).json({
        error: errorHandler(error),
      });
    }
    res.json(data);
  });
};

exports.createOrUpdate = (req, res) => {
  Favorite.findOne({user : req.params.userId}).exec((err, favorite) => {
    if (err || !favorite) {
      const favorite = new Favorite(req.body);
      favorite.save((error, data) => {
        if (error) {
          return res.status(400).json({
            error: errorHandler(error),
          });
        }
        res.json(data);
      });
    } else {
        Favorite.update(
            { user: req.params.userId },
            { $push: { "products": req.body.products}},
            (err, favorite) => {
              if (err) {
                return res.status(400).json({
                  error: errorHandler(err),
                });
              }
              res.json(favorite);
            }
          );
    }
    req.favorite = favorite;
  });
};

exports.list = (req, res) => {
  Favorite.findOne({ user: req.params.userId })
    .populate("products.productId")
    .exec((err, favorites) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler(error),
        });
      }
      res.json(favorites);
    });
};

exports.listFavorites = (req, res) => {
  Favorite.findOne({ user: req.params.userId })
    .populate("products.productId")
    .exec((err, favorites) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler(error),
        });
      }
      res.json(favorites);
    });
};

exports.getStatusValues = (req, res) => {
  res.json(Favorite.schema.path("status").enumValues);
};

exports.updateFavoriteStatus = (req, res) => {
  Favorite.update(
    { _id: req.body.favoriteId },
    { $set: { status: req.body.status } },
    (err, favorite) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler(err),
        });
      }
      res.json(favorite);
    }
  );
};
