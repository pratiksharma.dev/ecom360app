const mongoose = require("mongoose");
const {ObjectId}=mongoose.Schema;
const companySchema=new mongoose.Schema({
    name:{
        type:String,
        trim:true, // remove space from end & an begining if have
        required:true,
        maxlength:35
    },
    description:{
        type:String,
        required:true,
        maxlength:3000
    },
    companyEmail: {
        type: String,
        trim: true, // remove space from end & an begining if have
        required: true,
        unique: 35,
    },
    virtualTourUrl: {
        type: String,
        required: true,
    },
    photo:{
        data: Buffer,
        contentType: String
    }
    
},{timestamps:true});

module.exports= mongoose.model("Company",companySchema);