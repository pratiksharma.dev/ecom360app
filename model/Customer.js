const mongoose = require("mongoose");
const {ObjectId}=mongoose.Schema;
const customerSchema=new mongoose.Schema({
    member: { 
        type: ObjectId, 
        ref: "User",
        required: true
    },
    owner: {
        type:ObjectId,
        ref: "User",
        required: true
    },
    company: {
        type:ObjectId,
        ref:'Company',
    }
},{timestamps:true});

module.exports= mongoose.model("Customer",customerSchema);