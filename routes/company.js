const express = require("express");
const router=express.Router();

const {requireSignin,isAuth,isAdmin}=require('../controllers/auth');
const {userById}=require('../controllers/user');
const {create,companyById,photo,read,remove,update,list,companyByEmail}=require('../controllers/company');

router.get('/company/:companyId',read);
router.post('/company/create/:userId',requireSignin,isAuth,isAdmin,create);
router.delete('/company/:companyId/:userId',requireSignin,isAuth,isAdmin,remove)
router.put('/company/:companyId/:userId',requireSignin,isAuth,isAdmin,update)

router.get('/companies',list);
router.get('/company/by/:email',companyByEmail);
router.get("/company/photo/:companyId", photo);

router.param("userId",userById);
router.param("companyId",companyById);
module.exports=router;
