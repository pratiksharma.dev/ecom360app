const express = require("express");
const router=express.Router();

const {requireSignin,isAuth,isAdmin}=require('../controllers/auth');
const {userById}=require('../controllers/user');
const {create,customerById,photo,read,remove,update,list}=require('../controllers/customer');

router.get('/customer/:customerId',read);
router.post('/customer/create/:userId',requireSignin,isAuth,isAdmin,create);
router.delete('/customer/:customerId/:userId',requireSignin,isAuth,isAdmin,remove)
router.put('/customer/:customerId/:userId',requireSignin,isAuth,isAdmin,update)

router.get('/customers/:ownerId',list);

router.param("userId",userById);
router.param("customerId",customerById);
module.exports=router;