const express = require("express");
const router = express.Router();

const { requireSignin, isAuth,isAdmin } = require("../controllers/auth");
const { userById } = require("../controllers/user");
const { create, createOrUpdate,listFavorites,favoriteById,updateFavoriteStatus } = require("../controllers/favorite");


router.post(
  "/favorite/create/:userId",
  create
);

router.post(
  "/favorite/createOrUpdate/:userId",
  createOrUpdate
);

router.get('/favorites/list/:userId',listFavorites);

router.param("userId", userById);
router.param("favoriteById", favoriteById);
module.exports = router;
